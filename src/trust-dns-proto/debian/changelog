rust-trust-dns-proto (0.22.0-2) unstable; urgency=medium

  * Team upload.
  * Build-Depends on rust-ring to ensure we only provide packages on architectures
    that provide all build depends, Closes: #1026769
  * Package trust-dns-proto 0.22.0 from crates.io using debcargo 2.6.0

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 09 Jan 2023 16:04:16 -0500

rust-trust-dns-proto (0.22.0-1) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.22.0 from crates.io using debcargo 2.6.0
  * Disable quic support until the `quinn` crate is available in Debian
  * Downgrade dependency on enum-as-inner down to version 0.4

 -- Reinhard Tartler <siretart@tauware.de>  Fri, 16 Dec 2022 07:15:11 -0500

rust-trust-dns-proto (0.21.2-6) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0
  * Bump idna to 0.3. Thanks to Fabian Grünbichler.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 24 Oct 2022 06:04:59 +0000

rust-trust-dns-proto (0.21.2-5) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0
  * Revert overriding generated debian/control and debian/tests/control
  * Add rustls and webpki as dependency to the default feature so that it makes it to build depends
  * disable dnssec-ring
  * disable test that requires webpki-roots

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 04 Aug 2022 11:52:01 +0200

rust-trust-dns-proto (0.21.2-4) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0
  * Limit rustls and ring related depends/provides/autopkgtests to architectures
    where rustls and ring are available.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 02 Aug 2022 02:59:51 +0000

rust-trust-dns-proto (0.21.2-3) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0
  * build againt 'backtrace'
  * mark some tests as flaky

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 31 Jul 2022 09:56:46 +0200

rust-trust-dns-proto (0.21.2-2) unstable; urgency=medium

  * Team upload.
  * avoid depedency on 'backtrace'
  * update depend on rustls-pemfile to verison 1
  * avoid dependency on webpki-roots, we have certificates in the 'ca-certificates'
    package -- cf. discussion in #972802
  * avoid dependency on tokio-openssl, to be packaged
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0

 -- Reinhard Tartler <siretart@tauware.de>  Wed, 27 Jul 2022 22:12:17 +0200

rust-trust-dns-proto (0.21.2-1) unstable; urgency=medium

  * Team upload.
  * Package trust-dns-proto 0.21.2 from crates.io using debcargo 2.5.0
  * Set collapse_features = true

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 24 Jul 2022 08:46:41 +0200

rust-trust-dns-proto (0.8.0-1) unstable; urgency=medium

  * Package trust-dns-proto 0.8.0 from crates.io using debcargo 2.4.0

 -- kpcyrd <git@rxv.cc>  Sat, 02 Nov 2019 10:15:46 +0100
